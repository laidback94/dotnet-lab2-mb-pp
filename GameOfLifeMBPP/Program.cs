﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLifeMBPP
{
    class Program
    {
        static void Main(string[] args)
        {
            int width = 10;
            int height = 10;

            MatrixOfCell cellMatrix = new MatrixOfCell(width, height);

            while (true)
            {
                cellMatrix.ShowMatrix();
                System.Threading.Thread.Sleep(1000);
                cellMatrix.NextStep();
                Console.Clear();
            }

        }
    }
}
