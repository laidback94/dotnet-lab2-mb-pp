﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public enum State
{
    DEAD = 0,
    ALIVE = 1 
};

namespace GameOfLifeMBPP
{
    class Cell
    {
        private int numberALIVENeighbours;
        private State state;

        public Cell(Random rand)
        {
            state = (State)rand.Next(0, 2);
            numberALIVENeighbours = 0;
        }

        public Cell()
        {
            state = State.DEAD;
            numberALIVENeighbours = 0;
        }

        public void setState(State state)
        {
            this.state = state;
        }
        public State getState() {
            return state;
        }

        public void SetNumberAliveNeighbours(int numberALIVENeighbours)
        {
            this.numberALIVENeighbours = numberALIVENeighbours;
        }

        public State GetNextState()
        {
            if (numberALIVENeighbours < 2 || numberALIVENeighbours > 3)
            {
                return State.DEAD;
            }
            else
                return State.ALIVE;
        }

    }

}
