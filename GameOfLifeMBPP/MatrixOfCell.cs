﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLifeMBPP
{
    public class MatrixOfCell
    {
        private int borderWidth, borderHeight;
        private int startWidth, endWidth, startHeight, endHeight;
        private const int invisibleBorderSize = 1;
        Random rand;

        Cell[,] cellArray;

        public MatrixOfCell(int width = 10, int height = 10)
        {
            this.borderWidth = width + 2 * invisibleBorderSize; // left and right side
            this.borderHeight = height + 2 * invisibleBorderSize; //top and bottom

            this.startWidth = 1;
            this.endWidth = width;
            this.startHeight = 1;
            this.endHeight = height;

            cellArray = new Cell[borderWidth, borderHeight];
            rand = new Random();

            for (int x = 0; x < borderWidth; x++)
                for (int y = 0; y < borderHeight; y++)
                {
                    cellArray[x, y] = new Cell(rand);
                    if (x == 0 || y == 0 || borderWidth - 1 == 0 || borderHeight - 1 == 0)
                        cellArray[x, y].setState(State.DEAD); // resolving problem with calculate number of neighbors on edges
                }
        }

        public void NextStep()
        {

            Cell[,] newCellArray = new Cell[borderWidth, borderHeight];
            for (int x = 0; x < borderWidth; x++)
                for (int y = 0; y < borderHeight; y++)
                {
                    newCellArray[x, y] = new Cell();
                }

            calculateNumberOfNeighbors();

            for (int a = startWidth; a <= endWidth; a++)
                for (int b = startHeight; b <= endHeight; b++)
                {
                    newCellArray[a, b].setState(cellArray[a, b].GetNextState());
                }

            cellArray = newCellArray;
        }

        private int calculateNumberOfNeighbors()
        {
            for (int a = startWidth; a <= endWidth; a++)
            {
                for (int b = startHeight; b <= endHeight; b++)
                {
                    int aliveNeighbors = 0;

                    if (cellArray[a - 1, b - 1].getState() == State.ALIVE) aliveNeighbors++;
                    if (cellArray[a - 1, b].getState() == State.ALIVE) aliveNeighbors++;
                    if (cellArray[a - 1, b + 1].getState() == State.ALIVE) aliveNeighbors++;
                    if (cellArray[a, b - 1].getState() == State.ALIVE) aliveNeighbors++;
                    if (cellArray[a, b + 1].getState() == State.ALIVE) aliveNeighbors++;
                    if (cellArray[a + 1, b - 1].getState() == State.ALIVE) aliveNeighbors++;
                    if (cellArray[a + 1, b].getState() == State.ALIVE) aliveNeighbors++;
                    if (cellArray[a + 1, b + 1].getState() == State.ALIVE) aliveNeighbors++;

                    cellArray[a, b].SetNumberAliveNeighbours(aliveNeighbors);
                }
            }
            return 0;
        }

        public void ShowMatrix()
        {
            for (int a = startWidth; a <= endWidth; a++)
            {
                for (int b = startHeight; b <= endHeight; b++)
                {
                    Console.Out.Write((int)cellArray[a, b].getState());
                    Console.Out.Write(" ");
                }

                Console.Out.WriteLine();
            }
        }

    }
}
